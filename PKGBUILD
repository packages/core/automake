# Maintainer: Lukas Fleischer <lfleischer@archlinux.org>
# Contributor: Allan McRae <allan@archlinux.org>
# Contributor: judd <jvinet@zeroflux.org>

pkgname=automake
pkgver=1.16.1
pkgrel=0.1
pkgdesc="A GNU tool for automatically creating Makefiles"
arch=('any')
license=('GPL')
url="http://www.gnu.org/software/automake"
groups=('base-devel')
depends=('perl' 'bash')
makedepends=('autoconf')
checkdepends=('dejagnu' 'gcc-fortran' 'java-environment' 'vala' 'emacs' 'cscope')  # 'python'
source=(ftp://ftp.gnu.org/gnu/${pkgname}/${pkgname}-${pkgver}.tar.xz{,.sig}
        automake-1.15-dejagnu-testcase.patch
        0005-Disable-t-check12.sh-and-t-check12-w.sh.patch)
md5sums=('53f38e7591fa57c3d2cee682be668e5b'
         'SKIP'
         'ffa4dd8eb78cea82c3009d76087598b1'
         '26376b083cd2abf69724c3162d0328b3')
validpgpkeys=('E1622F96D2BB4E58018EEF9860F906016E407573'   # Stefano Lattarini
              'F2A38D7EEB2B66405761070D0ADEE10094604D37')  # Mathieu Lirzin

prepare() {
  cd ${srcdir}/${pkgname}-${pkgver}

  # fix testsuite issue with resent dejagnu
  patch -p1 -i $srcdir/automake-1.15-dejagnu-testcase.patch
  patch -p1 -i $srcdir/0005-Disable-t-check12.sh-and-t-check12-w.sh.patch
}

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  ./configure --build=$CHOST --prefix=/usr
  make
}

check() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  make check
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  make DESTDIR="${pkgdir}" install
}
